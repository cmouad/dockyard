package com.nespresso.exercise.dockyard;

public class Elevator {

	private AbstractElevatorAction elevatorAction;

	public void setElevatorAction(AbstractElevatorAction elevatorAction) {
		this.elevatorAction = elevatorAction;
	}

	public void work() {
		elevatorAction.work();
	}

//	public int[][] getStatus() {
//		return elevatorAction.getStatus();
//	}

}
