package com.nespresso.exercise.dockyard;

public interface IPrinter {

	void append(int data);
	String print();
}
