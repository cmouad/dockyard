package com.nespresso.exercise.dockyard;

public class TextPrinter implements IPrinter {

	private int datas[] = new int[3];
	private int i;

	public void append(int data) {
		if (i < 3) {
			datas[i] = data;
			i++;
		}
	}

	public String print() {
		StringBuilder result = new StringBuilder();
		int maxData = Math.max(datas[0], Math.max(datas[1], datas[2]));
		printShipSource(result, maxData);
		printDockyar(result, maxData);
		printShipDestination(result, maxData);
		return result.toString();
	}

	private void printShipSource(StringBuilder result, int maxData) {
		StringBuilder result1 = new StringBuilder();
		int j = 0;
		while (j < maxData) {
			if (j < datas[0])
				result1.append("X \n");
			else
				result1.append("  \n");
			j++;
		}
		result1.reverse();
//		result1.append("\n");
		result.append(result1);
	}

	private void printDockyar(StringBuilder result, int maxData) {
		StringBuilder result1 = new StringBuilder();
		int j = 0;
		while (j < maxData) {
			if (j < datas[1])
				result.append("X \n");
			else
				result.append("  \n");
			j++;
		}
		result1.reverse();
//		result1.append("\n");
		result.append(result1);
	}

	private void printShipDestination(StringBuilder result, int maxData) {
		StringBuilder result1 = new StringBuilder();
		int j = 0;
		while (j < maxData) {
			if (j < datas[2])
				result.append("X\n");
			else
				result.append(" \n");
			j++;
		}
		result1.reverse();
//		result1.append("\n");
		result.append(result1);
	}

	public static void main(String[] args) {
		StringBuilder result = new StringBuilder("X    \nX    \nX X  \n");
		System.out.println(result.reverse().toString());
	}

}
