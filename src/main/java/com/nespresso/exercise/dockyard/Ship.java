package com.nespresso.exercise.dockyard;

public class Ship implements Loadable, UnLoadable {

	private final int capacity;
	private int payload;

	public Ship(int capacity) {
		this.capacity = capacity;
		this.payload = 0;
	}

	public Ship(int capacity, int payload) {
		this.capacity = capacity;
		this.payload = payload;
	}

	public void load() {
		payload++;
	}

	public void unload() {
		payload--;
	}

	public boolean canUnload() {
		return payload > 0;
	}

	public boolean canLoad() {
		return payload < capacity;
	}

	public int getStatus() {
		return payload;
	}

	public void print(IPrinter printer) {
		printer.append(payload);
	}
}
